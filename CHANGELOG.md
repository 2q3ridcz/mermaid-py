# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd
### Fixed
- Link related bug in Flowchart.Node

## [0.1.1] - 2023-05-22
### Added
- Devcontainer
- GitLab CI

## [0.1.0] - 2023-01-16
### Added
- flowchart

[Unreleased]: https://gitlab.com/2q3ridcz/mermaid-py/-/compare/v0.1.1...main
[0.1.1]: https://gitlab.com/2q3ridcz/mermaid-py/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/2q3ridcz/mermaid-py/-/tree/v0.1.0

import pytest

from mermaidpy import flowchart as fc


class TestMermaidDocs():
    def test_diagrams_that_mermaid_can_render(self, html_dict):
        '''https://mermaid-js.github.io/mermaid/#/

        graph TD;
            A-->B;
            A-->C;
            B-->D;
            C-->D;
        '''
        # given
        expect = [
            'flowchart TD',
            'A --> B',
            'A --> C',
            'B --> D',
            'C --> D',
        ]

        # when
        flow = fc.Flowchart(direction="TD")
        flow.add_edge(fc.Edge(edge_id="e1", previous="A", following="B"))
        flow.add_edge(fc.Edge(edge_id="e2", previous="A", following="C"))
        flow.add_edge(fc.Edge(edge_id="e3", previous="B", following="D"))
        flow.add_edge(fc.Edge(edge_id="e4", previous="C", following="D"))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'graph TD;',
            '    A-->B;',
            '    A-->C;',
            '    B-->D;',
            '    C-->D;',
        ]
        html_dict['title'] = "test_diagrams_that_mermaid_can_render"

    def test_entity_codes_to_escape_characters(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#entity-codes-to-escape-characters

            flowchart LR
                A["A double quote:#quot;"] -->B["A dec char:#9829;"]
        '''
        # given
        expect = [
            'flowchart LR',
            'A["A double quote:#quot;"]',
            'B["A dec char:#9829;"]',
            'A --> B',
        ]

        # when
        flow = fc.Flowchart(direction="LR")
        flow.add_node(fc.Node(node_id="A", label="A double quote:#quot;"))
        flow.add_node(fc.Node(node_id="B", label="A dec char:#9829;"))
        flow.add_edge(fc.Edge(edge_id="e1", previous="A", following="B"))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart LR',
            '    A["A double quote:#quot;"] -->B["A dec char:#9829;"]',
        ]
        html_dict['title'] = "test_entity_codes_to_escape_characters"

    def test_subgraphs(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#subgraphs

        flowchart TB
            c1-->a2
            subgraph one
            a1-->a2
            end
            subgraph two
            b1-->b2
            end
            subgraph three
            c1-->c2
            end
        '''
        # given
        expect = [
            'flowchart TB',
            'subgraph one ["one"]',
            'a1["a1"]',
            'a2["a2"]',
            'end',
            'subgraph two ["two"]',
            'b1["b1"]',
            'b2["b2"]',
            'end',
            'subgraph three ["three"]',
            'c1["c1"]',
            'c2["c2"]',
            'end',
            'c1 --> a2',
            'a1 --> a2',
            'b1 --> b2',
            'c1 --> c2',
        ]

        # when
        flow = fc.Flowchart(direction="TB")
        one = fc.Node(node_id="one")
        two = fc.Node(node_id="two")
        three = fc.Node(node_id="three")
        one.add_child(fc.Node(node_id="a1"))
        one.add_child(fc.Node(node_id="a2"))
        two.add_child(fc.Node(node_id="b1"))
        two.add_child(fc.Node(node_id="b2"))
        three.add_child(fc.Node(node_id="c1"))
        three.add_child(fc.Node(node_id="c2"))
        flow.add_node(one)
        flow.add_node(two)
        flow.add_node(three)
        flow.add_edge(fc.Edge(edge_id="e1", previous="c1", following="a2"))
        flow.add_edge(fc.Edge(edge_id="e2", previous="a1", following="a2"))
        flow.add_edge(fc.Edge(edge_id="e3", previous="b1", following="b2"))
        flow.add_edge(fc.Edge(edge_id="e4", previous="c1", following="c2"))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart TB',
            '    c1-->a2',
            '    subgraph one',
            '    a1-->a2',
            '    end',
            '    subgraph two',
            '    b1-->b2',
            '    end',
            '    subgraph three',
            '    c1-->c2',
            '    end',
        ]
        html_dict['title'] = "test_subgraphs"

    def test_you_can_also_set_an_excplicit_id_for_the_subgraph(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#subgraphs

        flowchart TB
            c1-->a2
            subgraph ide1 [one]
            a1-->a2
            end
        '''
        # given
        expect = [
            'flowchart TB',
            'c1["c1"]',
            'subgraph ide1 ["one"]',
            'a1["a1"]',
            'a2["a2"]',
            'end',
            'c1 --> a2',
            'a1 --> a2',
        ]

        # when

        # Crate an empty flowchart object.
        flow = fc.Flowchart()

        # Create nodes as node objects.
        node_a1 = fc.Node(node_id="a1")
        node_a2 = fc.Node(node_id="a2")
        node_c1 = fc.Node(node_id="c1")

        # Create subgraphs as node objects.
        subgraph_ide1 = fc.Node(node_id="ide1", label="one")

        # Add child nodes or subgraphs to their parent subgraph.
        subgraph_ide1.add_child(node=node_a1)
        subgraph_ide1.add_child(node=node_a2)

        # Create edges as edge objects.
        edge_c1_a2 = fc.Edge(edge_id="c1_a2", previous="c1", following="a2")
        edge_a1_a2 = fc.Edge(edge_id="a1_a2", previous="a1", following="a2")

        # Add nodes, subgraphs, and edges to the flowchart object.
        # Child nodes or subgraphs do not need to be added here,
        # since they are added to their parent subgraphs already.
        flow.add_node(node=node_c1)
        flow.add_node(node=subgraph_ide1)
        flow.add_edge(edge=edge_c1_a2)
        flow.add_edge(edge=edge_a1_a2)

        # Convert flowchart object to mermaid string.
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart TB',
            '    c1-->a2',
            '    subgraph ide1 [one]',
            '    a1-->a2',
            '    end',
        ]
        html_dict['title'] = "test_you_can_also_set_an_excplicit_id_for_the_subgraph"

    def test_link_target_to_the_click_definition(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#interaction

        flowchart LR
            A-->B
            B-->C
            C-->D
            D-->E
            click A "https://www.github.com" _blank
            click B "https://www.github.com" "Open this in a new tab" _blank
            click C href "https://www.github.com" _blank
            click D href "https://www.github.com" "Open this in a new tab" _blank
        '''
        # given
        expect = [
            'flowchart LR',
            'A["A"]',
            'click A "https://www.github.com" _blank',
            'B["B"]',
            'click B "https://www.github.com" "Open this in a new tab" _blank',
            'C["C"]',
            'click C "https://www.github.com" _blank',
            'D["D"]',
            'click D "https://www.github.com" "Open this in a new tab" _blank',
            'A --> B',
            'B --> C',
            'C --> D',
            'D --> E',
        ]

        # when
        flow = fc.Flowchart(direction="LR")
        flow.add_node(fc.Node(
            node_id="A",
            link="https://www.github.com",
            link_target="_blank",
        ))
        flow.add_node(fc.Node(
            node_id="B",
            link="https://www.github.com",
            link_target="_blank",
            tooltip="Open this in a new tab",
        ))
        flow.add_node(fc.Node(
            node_id="C",
            link="https://www.github.com",
            link_target="_blank",
        ))
        flow.add_node(fc.Node(
            node_id="D",
            link="https://www.github.com",
            link_target="_blank",
            tooltip="Open this in a new tab",
        ))
        flow.add_edge(fc.Edge(edge_id="e1", previous="A", following="B"))
        flow.add_edge(fc.Edge(edge_id="e2", previous="B", following="C"))
        flow.add_edge(fc.Edge(edge_id="e3", previous="C", following="D"))
        flow.add_edge(fc.Edge(edge_id="e4", previous="D", following="E"))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart LR',
            '    A-->B',
            '    B-->C',
            '    C-->D',
            '    D-->E',
            '    click A "https://www.github.com" _blank',
            '    click B "https://www.github.com" "Open this in a new tab" _blank',
            '    click C href "https://www.github.com" _blank',
            '    click D href "https://www.github.com" "Open this in a new tab" _blank',
        ]
        html_dict['title'] = "test_link_target_to_the_click_definition"

    def test_link_target_to_the_click_definition_with_different_target(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#interaction

        Changed B, C, D to link target other than _blank.
        Added F, link with no link target.

        flowchart LR
            A-->B
            B-->C
            C-->D
            D-->E
            E-->F
            click A "https://www.github.com" _blank
            click B "https://www.github.com" "Open this in a new tab" _self
            click C href "https://www.github.com" _parent
            click D href "https://www.github.com" "Open this in a new tab" _top
            click F "https://www.github.com" 
        '''
        # given
        expect = [
            'flowchart LR',
            'A["A"]',
            'click A "https://www.github.com" _blank',
            'B["B"]',
            'click B "https://www.github.com" "Open this in a new tab" _self',
            'C["C"]',
            'click C "https://www.github.com" _parent',
            'D["D"]',
            'click D "https://www.github.com" "Open this in a new tab" _top',
            'F["F"]',
            'click F "https://www.github.com"',
            'A --> B',
            'B --> C',
            'C --> D',
            'D --> E',
            'E --> F',
        ]

        # when
        flow = fc.Flowchart(direction="LR")
        flow.add_node(fc.Node(
            node_id="A",
            link="https://www.github.com",
            link_target="_blank",
        ))
        flow.add_node(fc.Node(
            node_id="B",
            link="https://www.github.com",
            link_target="_self",
            tooltip="Open this in a new tab",
        ))
        flow.add_node(fc.Node(
            node_id="C",
            link="https://www.github.com",
            link_target="_parent",
        ))
        flow.add_node(fc.Node(
            node_id="D",
            link="https://www.github.com",
            link_target="_top",
            tooltip="Open this in a new tab",
        ))
        flow.add_node(fc.Node(
            node_id="F",
            link="https://www.github.com",
        ))
        flow.add_edge(fc.Edge(edge_id="e1", previous="A", following="B"))
        flow.add_edge(fc.Edge(edge_id="e2", previous="B", following="C"))
        flow.add_edge(fc.Edge(edge_id="e3", previous="C", following="D"))
        flow.add_edge(fc.Edge(edge_id="e4", previous="D", following="E"))
        flow.add_edge(fc.Edge(edge_id="e5", previous="E", following="F"))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart LR',
            '    A-->B',
            '    B-->C',
            '    C-->D',
            '    D-->E',
            '    E-->F',
            '    click A "https://www.github.com" _blank',
            '    click B "https://www.github.com" "Open this in a new tab" _self',
            '    click C href "https://www.github.com" _parent',
            '    click D href "https://www.github.com" "Open this in a new tab" _top',
            '    click F "https://www.github.com"',
        ]
        html_dict['title'] = "test_link_target_to_the_click_definition_with_different_target"

    def test_graph_declarations_with_spaces_between(self, html_dict):
        '''https://mermaid.js.org/syntax/flowchart.html#graph-declarations-with-spaces-between-vertices-and-link-and-without-semicolon

        flowchart LR
            A[Hard edge] -->|Link text| B(Round edge)
            B --> C{Decision}
            C -->|One| D[Result one]
            C -->|Two| E[Result two]

        Expect is different because node shape is not implemented in pymermaid.
        '''
        # given
        expect = [
            'flowchart LR',
            'A["Hard edge"]',
            'B["Round edge"]',
            'C["Decision"]',
            'D["Result one"]',
            'E["Result two"]',
            'A -- Link text --> B',
            'B --> C',
            'C -- One --> D',
            'C -- Two --> E'
        ]

        # when
        flow = fc.Flowchart(direction="LR")
        flow.add_node(fc.Node(node_id="A", label="Hard edge"))
        flow.add_node(fc.Node(node_id="B", label="Round edge"))
        flow.add_node(fc.Node(node_id="C", label="Decision"))
        flow.add_node(fc.Node(node_id="D", label="Result one"))
        flow.add_node(fc.Node(node_id="E", label="Result two"))
        flow.add_edge(fc.Edge(
            edge_id="e1",
            previous="A",
            following="B",
            label="Link text"
        ))
        flow.add_edge(fc.Edge(edge_id="e2", previous="B", following="C"))
        flow.add_edge(fc.Edge(
            edge_id="e3",
            previous="C",
            following="D",
            label="One"
        ))
        flow.add_edge(fc.Edge(
            edge_id="e4",
            previous="C",
            following="E",
            label="Two"
        ))
        mmd = flow.to_mermaid()

        # then
        assert mmd == expect

        # for doc
        html_dict['diagram'] = flow
        html_dict['compare_code'] = [
            'flowchart LR',
            '    A[Hard edge] -->|Link text| B(Round edge)',
            '    B --> C{Decision}',
            '    C -->|One| D[Result one]',
            '    C -->|Two| E[Result two]',
        ]
        html_dict['title'] = "test_graph_declarations_with_spaces_between"

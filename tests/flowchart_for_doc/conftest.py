import pytest

import pathlib

from mermaidpy.presentation import MermaidDocSampleTestHtml


@pytest.fixture(scope='function', autouse=True)
def html_dict():
    html_dict = {}
    yield html_dict

    diagram = html_dict['diagram']
    compare_code = html_dict['compare_code']
    title = html_dict['title']

    SELF_FILE = pathlib.Path(__file__).resolve()
    output_folder = SELF_FILE.parent.joinpath('output')

    for ver in [
        "latest",
        "9.3.0",
        "8.3.1",
    ]:
        ver_folder = output_folder.joinpath(ver)
        output_file = ver_folder.joinpath(f'{title}.html')

        if ver == "latest":
            src = "https://unpkg.com/mermaid/dist/mermaid.min.js"
        else:
            src = f"https://unpkg.com/mermaid@{ver}/dist/mermaid.min.js"

        # for doc
        writer = MermaidDocSampleTestHtml(
            diagram=diagram,
            compare_code=compare_code,
            title=title,
            mermaid_src=src,
        )
        ver_folder.mkdir(exist_ok=True)
        writer.to_file(path=str(output_file.resolve()))

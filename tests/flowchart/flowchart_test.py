import pytest

from mermaidpy import flowchart as fc


class TestFlowchart():
    def test_add_node(self):
        flow = fc.Flowchart()
        assert len(flow.nodes) == 0
        node001 = fc.Node(node_id="node001", label="label001")
        flow.add_node(node001)
        assert len(flow.nodes) == 1
        child01 = fc.Node(node_id="child01", label="child01")
        node002 = fc.Node(node_id="node002", label="label002")
        node002.add_child(node=child01)
        flow.add_node(node002)
        assert len(flow.nodes) == 2

        assert flow.nodes[node001.node_id].node_id == node001.node_id
        assert flow.nodes[node001.node_id].label == node001.label
        assert flow.nodes[node001.node_id].children == node001.children
        assert flow.get_node("node001").node_id == node001.node_id
        assert flow.get_node("node001").label == node001.label
        assert flow.get_node("node001").children == node001.children

        assert flow.nodes[node002.node_id].node_id == node002.node_id
        assert flow.nodes[node002.node_id].label == node002.label
        assert flow.nodes[node002.node_id].children == node002.children
        assert flow.get_node("node002").node_id == node002.node_id
        assert flow.get_node("node002").label == node002.label
        assert flow.get_node("node002").children == node002.children

        assert flow.get_node("node000") is None
        with pytest.raises(KeyError):
            flow.delete_node("node000")

        deleted_node = flow.delete_node("node001")
        assert deleted_node.node_id == "node001"
        assert len(flow.nodes) == 1

        assert flow.get_node("node001") is None
        assert flow.get_node("node002") is not None

    def test_add_edge(self):
        flow = fc.Flowchart()
        assert len(flow.edges) == 0
        edge001 = fc.Edge(edge_id="e1", previous="p1", following="f1")
        flow.add_edge(edge001)
        assert len(flow.edges) == 1
        edge002 = fc.Edge(edge_id="e", previous="p", following="f", label="l")
        flow.add_edge(edge002)
        assert len(flow.edges) == 2

        assert flow.edges[edge001.edge_id].edge_id == edge001.edge_id
        assert flow.edges[edge001.edge_id].previous == edge001.previous
        assert flow.edges[edge001.edge_id].following == edge001.following
        assert flow.edges[edge001.edge_id].label == edge001.label
        assert flow.get_edge("e1").edge_id == edge001.edge_id
        assert flow.get_edge("e1").previous == edge001.previous
        assert flow.get_edge("e1").following == edge001.following
        assert flow.get_edge("e1").label == edge001.label

        assert flow.edges[edge002.edge_id].edge_id == edge002.edge_id
        assert flow.edges[edge002.edge_id].previous == edge002.previous
        assert flow.edges[edge002.edge_id].following == edge002.following
        assert flow.edges[edge002.edge_id].label == edge002.label
        assert flow.get_edge("e").edge_id == edge002.edge_id
        assert flow.get_edge("e").previous == edge002.previous
        assert flow.get_edge("e").following == edge002.following
        assert flow.get_edge("e").label == edge002.label

        assert flow.get_edge("edge000") is None
        with pytest.raises(KeyError):
            flow.delete_node("edge000")

        deleted_edge = flow.delete_edge("e1")
        assert deleted_edge.edge_id == "e1"
        assert len(flow.edges) == 1

        assert flow.get_edge("e1") is None
        assert flow.get_edge("e") is not None

# mermaid-py

Write mermaid.js diagrams from python programs.

## usage

### Flowchart

Let's try and create this sample diagram from mermaid.js document.

```mermaid
flowchart TB
    c1-->a2
    subgraph ide1 [one]
    a1-->a2
    end
```
```
flowchart TB
    c1-->a2
    subgraph ide1 [one]
    a1-->a2
    end
```

We will look at the code later.
First, take a look at result:

```
flowchart TB
c1["c1"]
subgraph ide1 ["one"]
a1["a1"]
a2["a2"]
end
c1 --> a2
a1 --> a2
```

This is not exactly same as the target, but draws the same diagram.

```mermaid
flowchart TB
c1["c1"]
subgraph ide1 ["one"]
a1["a1"]
a2["a2"]
end
c1 --> a2
a1 --> a2
```

Things to do are:
1. Create an empty flowchart object.
2. Create nodes as node objects. (a1, a2, c1)
3. Create subgraphs as node objects. (ide1)
4. Add child nodes or subgraphs to their parent subgraph.
    (Add a1 and a2 to ide1)
5. Create edges as edge objects. (c1-->a2, a1-->a2)
6. Add nodes, subgraphs, and edges to the flowchart object.
    (c1, ide1, c1-->a2, a1-->a2)
    Child nodes or subgraphs do not need to be added here, since they are added to their parent subgraphs already.
7. Convert flowchart object to mermaid string.

Note on subgraphs.
Subgraphs are a kind of node which has children.
Use node object to create a subgraph.
In mermaid-py, node objects are treated as nodes when they have no children, and treated as subgraphs when they have children.

Here is the code:

```python
from mermaidpy import flowchart as fc

# Crate an empty flowchart object.
flow = fc.Flowchart()

# Create nodes as node objects.
node_a1 = fc.Node(node_id="a1")
node_a2 = fc.Node(node_id="a2")
node_c1 = fc.Node(node_id="c1")

# Create subgraphs as node objects.
subgraph_ide1 = fc.Node(node_id="ide1", label="one")

# Add child nodes or subgraphs to their parent subgraph.
subgraph_ide1.add_child(node=node_a1)
subgraph_ide1.add_child(node=node_a2)

# Create edges as edge objects.
edge_c1_a2 = fc.Edge(edge_id="c1_a2", previous="c1", following="a2")
edge_a1_a2 = fc.Edge(edge_id="a1_a2", previous="a1", following="a2")

# Add nodes, subgraphs, and edges to the flowchart object.
# Child nodes or subgraphs do not need to be added here,
# since they are added to their parent subgraphs already.
flow.add_node(node=node_c1)
flow.add_node(node=subgraph_ide1)
flow.add_edge(edge=edge_c1_a2)
flow.add_edge(edge=edge_a1_a2)

# Convert flowchart object to mermaid string.
mmd = flow.to_mermaid()
```

## class diagram

```mermaid
flowchart TB
subgraph class-diagram
subgraph mermaidpy
subgraph flowchart
FlowChart["Flowchart<br/>---<br/>direction<br/>nodes<br/>edges<br/>---<br/>to_mermaid()"]
Node["Node<br/>---<br/>node_id<br/>label<br/>link<br/>link_target<br/>tooltip<br/>children<br/>---<br/>to_mermaid()"]
Edge["Edge<br/>---<br/>edge_id<br/>label<br/>previous<br>following<br/>---<br/>to_mermaid()"]
end
subgraph presentation
SimpleHtml["SimpleHtml<br/>---<br/>diagram<br/>"]
end
subgraph Diagram
diagram["Diagram<br/>---<br/>---<br/>to_mermaid()"]
end
end
end
FlowChart --> diagram
FlowChart --> Edge
FlowChart --> Node
SimpleHtml --> diagram
```

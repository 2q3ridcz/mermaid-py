"""Flowchart.Flowchart"""
import typing
from dataclasses import dataclass, field

from ..diagram.diagram import Diagram
from .edge import Edge
from .node import Node


@dataclass(init=True, eq=True, frozen=False)
class Flowchart(Diagram):
    """Flowchart.Flowchart"""
    direction: str = "TB"
    nodes: typing.Dict[str, Node] = field(default_factory=dict)
    edges: typing.Dict[str, Edge] = field(default_factory=dict)

    def add_node(self, node: Node) -> None:
        self.nodes[node.node_id] = node

    def get_node(self, node_id: str) -> typing.Optional[Node]:
        return self.nodes.get(node_id)

    def delete_node(self, node_id: str) -> Node:
        return self.nodes.pop(node_id)

    def add_edge(self, edge: Edge) -> None:
        self.edges[edge.edge_id] = edge

    def get_edge(self, edge_id: str) -> typing.Optional[Edge]:
        return self.edges.get(edge_id)

    def delete_edge(self, edge_id: str) -> Edge:
        return self.edges.pop(edge_id)

    def to_mermaid(self) -> typing.List[str]:
        direction = self.direction
        nodes = self.nodes
        edges = self.edges

        lines = []
        lines.append("flowchart " + direction)
        for node in nodes.values():
            lines += node.to_mermaid()
        for edge in edges.values():
            lines += edge.to_mermaid()
        return lines

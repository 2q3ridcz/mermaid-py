"""Flowchart.Edge"""
import typing
from dataclasses import dataclass


@dataclass(init=True, eq=True, frozen=True)
class Edge:
    """Flowchart.Edge"""
    edge_id: str
    previous: str
    following: str
    label: str = ""

    def to_mermaid(self) -> typing.List[str]:
        previous = self.previous
        following = self.following
        label = self.label

        arrow = "-->" if label == "" else f"-- {label} -->"
        return [f'{previous} {arrow} {following}']

"""Represents a node or a subgraph of a flowchart"""
import typing
from dataclasses import dataclass, field


@dataclass(init=True, eq=True, frozen=True)
class Node:
    """Represents a node or a subgraph of a flowchart.
    
    Attributes:
        node_id: Id of the node. Used as label if 'label' is not set.
        label: Text to display in the node.
        link: Url to open when the node is clicked.
        link_target:
            > Links are opened in the same browser tab/window by default.
            > It is possible to change this by adding a link target to the
            > click definition (_self, _blank, _parent and _top are supported).
        tooltip:
            Text to display while mouseover.
            Tooltip appears at the bottom of the page by default.
            Css and javascript are needed to change position.
        children: Dict of child nodes.
    """
    node_id: str
    label: str = ""
    link: str = ""
    link_target: str = ""
    tooltip: str = ""
    children: typing.Dict[str, "Node"] = field(default_factory=dict)

    def add_child(self, node: "Node") -> None:
        self.children[node.node_id] = node

    def get_child(self, node_id: str) -> "Node":
        return self.children[node_id]

    def delete_child(self, node_id: str) -> "Node":
        return self.children.pop(node_id)

    def to_mermaid(self) -> typing.List[str]:
        node_id = self.node_id
        label = self.label
        link = self.link
        link_target = self.link_target
        tooltip = self.tooltip
        children = self.children

        child_nodes = children.values()
        label_str = node_id if label == "" else label

        lines = []
        if len(child_nodes) == 0:
            # Treat as a node
            lines.append(f'{node_id}["{label_str}"]')

            if link != "":
                link_lines = [
                    "click",
                    node_id,
                    "" if link == "" else f'"{link}"',
                    "" if tooltip == "" else f'"{tooltip}"',
                    link_target,
                ]
                link_line = " ".join([x for x in link_lines if x != ""])
                lines.append(link_line)
            return lines
        else:
            # Treat as a subgraph
            lines.append(f'subgraph {node_id} ["{label_str}"]')
            for child in child_nodes:
                lines += child.to_mermaid()
            lines.append('end')
            return lines

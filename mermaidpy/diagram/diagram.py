from abc import ABC, abstractmethod
import typing


class Diagram(ABC):
    @abstractmethod
    def to_mermaid(self) -> typing.List[str]:
        """Return mermaid strings."""
        raise NotImplementedError

import pathlib
import typing

import jinja2

from ..diagram.diagram import Diagram


class MermaidDocSampleTestHtml:
    def __init__(
        self,
        diagram: Diagram,
        compare_code: typing.List[str],
        title: str,
        mermaid_src: str = 'https://unpkg.com/mermaid/dist/mermaid.min.js',
    ) -> None:
        self.diagram = diagram
        self.compare_code = compare_code
        self.title = title
        self.mermaid_src = mermaid_src

    def to_html(self) -> str:
        diagram = self.diagram
        compare_code = self.compare_code
        title = self.title
        mermaid_src = self.mermaid_src

        SELF_FILE = pathlib.Path(__file__).resolve()
        template_folder = SELF_FILE.parent.joinpath('templates')
        template_file = template_folder.joinpath('mermaid_doc_sample_test_html.j2.html')

        data = {
            'title': title,
            'compare_code': "\n".join(compare_code),
            'created_code': "\n".join(diagram.to_mermaid()),
            'mermaid_src': mermaid_src,
        }

        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(template_folder.resolve()))
        )
        template = env.get_template(template_file.name)
        rendered = template.render(data=data)
        return rendered

    def to_file(self, path: str) -> None:
        rendered = self.to_html()
        with open(path, 'w', encoding='utf-8') as f:
            f.write(rendered)
